program PingUtil;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,

  Winapi.Windows, Winapi.Messages, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, IdBaseComponent, IdComponent, IdRawBase,
  IdRawClient, IdIcmpClient;




function PingAttempt(IP: String; PkSize: Integer; Report: TStrings): String;
const
  PingCount = 120;   //���������� ������ � �������
var
  IdIcmpClient1: TIdIcmpClient;
  sTran, sTime, sRep: String;
  i: Integer;
  CountSuccess, TimeSuccess: Integer;
begin
  //��������� �������������
  IdIcmpClient1 := TIdIcmpClient.Create( nil );
  IdIcmpClient1.ReceiveTimeout := 1500;
  IdIcmpClient1.Host := IP;
  CountSuccess := 0;
  TimeSuccess  := 0;
  //������ ��������
  sTran := StringOfChar('0', PkSize);
  //������ 120 ������� �����
  for i := 1 to PingCount do begin
    //���������� ����
    sRep := '';
    try
      IdIcmpClient1.Ping( sTran );
    except
      on E: Exception do sRep := E.Message;
    end;
    //�������������� ������ �����
    if sRep <> '' then begin
      Report.Add( sRep );
      Continue;
    end;
    //����� �����
    if (IdIcmpClient1.ReplyStatus.MsRoundTripTime = 0) then sTime := '<1'
    else sTime := '=' + IdIcmpClient1.ReplyStatus.MsRoundTripTime.ToString;
    //������� ����� � ����
    Report.Add( '����� �� ' + IdIcmpClient1.ReplyStatus.FromIpAddress + ': ' +
       '����� ����=' + IdIcmpClient1.ReplyStatus.BytesReceived.ToString + ' ' +
       '�����' + sTime + ' ' +
       'TTL=' + IdIcmpClient1.ReplyStatus.TimeToLive.ToString);
    //������������ ������� �������
    if IdIcmpClient1.ReplyStatus.ReplyStatusType = rsEcho then begin
      Inc( CountSuccess );
      TimeSuccess := TimeSuccess + IdIcmpClient1.ReplyStatus.MsRoundTripTime;
    end;
  end;
  //��������� % �������� ������
  Result := '�������� ������ ' + CountSuccess.ToString + ' �� ' +
    PingCount.ToString + ' (' + Round(CountSuccess/PingCount*100).ToString +
    ' % ��������)';
  //��������� ������� ����� �������� ������
  if CountSuccess > 0 then
    Result := Result + sLineBreak + '������� ����� �������� ������: '
      + Round(TimeSuccess/CountSuccess).ToString;
  //����������� ������
  IdIcmpClient1.Free;
end;


const
  AttemptCount = 3;  //���������� ������� ������
var
  sResult: String;
  i: Integer;
  sl: TStringList;
  PkSize: Integer;
begin
  //��������
  if ParamCount < 2 then begin
    Writeln('�� ������ ������������ ���������');
    Exit;
  end;
  if not TryStrToInt( ParamStr(2), PkSize ) then begin
    Writeln('�� ����� ������ ������ ������');
    Exit;
  end;
  //��������� �����
  sl := TStringList.Create;
  for i := 1 to AttemptCount do begin
    sResult := PingAttempt(ParamStr(1), PkSize, sl);
    Writeln( sResult );
    //����� 5 ���
    if i < AttemptCount then Sleep( 5000 );
  end;
  //�������� ��������� ��� � ����
  if ParamCount > 2 then
    try
      sl.SaveToFile( ParamStr(3) );
    except
      On E: Exception do Writeln( E.Message );
    end;
  //����������
  sl.Free;
end.
